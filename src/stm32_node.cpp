#include "main.h"
#include "stm32_node.h"
#include <ros.h>
#include <std_msgs/Bool.h>

ros::NodeHandle nh;
void led_cb(const std_msgs::Bool& msg);
ros::Subscriber<std_msgs::Bool> led_pub("status_led", &led_cb);

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
  nh.getHardware()->flush();
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
  nh.getHardware()->reset_rbuf();
}

class Led
{
  private:
    GPIO_TypeDef* gpio;
    uint16_t pin;
  public:
    Led(GPIO_TypeDef* t_gpio, uint16_t t_pin);
    ~Led(){};
    void on();
    void off();
    void toggle();
    bool state();
};

Led led(LED_GREEN_GPIO_Port, LED_GREEN_Pin);


void start_STM32_Node()
{

    nh.initNode();
    nh.subscribe(led_pub);


    while (true)
    {
      nh.spinOnce();
      HAL_Delay(500); //loop rate
    }
}


void led_cb(const std_msgs::Bool& msg)
{
  if (msg.data)
  {
    led.on();
  }
  else
  {
    led.off();
  }
}


Led::Led(GPIO_TypeDef* t_gpio, uint16_t t_pin)
      : gpio(t_gpio)
      , pin(t_pin)
{
  this->on();
}

void Led::on()
{
  HAL_GPIO_WritePin(gpio, pin, GPIO_PIN_SET);
}
void Led::off()
{
  HAL_GPIO_WritePin(gpio, pin, GPIO_PIN_RESET);
}
void Led::toggle()
{
  HAL_GPIO_TogglePin(gpio, pin);
}
bool Led::state() 
{
  return HAL_GPIO_ReadPin(gpio, pin);
}
