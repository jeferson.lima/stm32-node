# stm32-node

This project brings support for the STM32G0071RB MCU to communicate with ROS1 system through a USART.



## Requeriments
* [ROS - Robot Operating System](https://www.ros.org/)
	* [rosserial_stm32](https://github.com/yoneken/rosserial_stm32)
	* [rosserial_python](http://wiki.ros.org/rosserial_python)
* [GNU Arm Embedded Toolchain](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)
* [ST-LINK server software](https://www.st.com/en/development-tools/st-link-server.html)
* [GDB dashboard](https://github.com/cyrus-and/gdb-dashboard)

## Quickstart

Just clone the project in your git directory:

```bash
$ git clone https://gitlab.com/jeferson.lima/stm32-node.git
```
Connect the stm32 board on usb and run the command:

```bash
$ make
```

## Usage

```bash
$ rosrun rosserial_python serial_node.py _port:=/dev/ttyACM0 _baud:=115200
```

```bash
$ # set led
$ rostopic pub /status_led std_msgs/Bool "data: true"
$ # reset led
$ rostopic pub /status_led std_msgs/Bool "data: false"
```

