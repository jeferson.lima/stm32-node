.PHONY: all build cmake clean format

TARGET = sample-g071rb
BUILD_DIR := build
BUILD_TYPE ?= Debug

ST_UTIL_PORT = 4242
ST_UTIL_CMD =  "-ex=target extended-remote localhost:${ST_UTIL_PORT}"
ST_UTIL_CMD += "-ex=dashboard assembly -style height 5"
ST_UTIL_CMD += "-ex=dashboard source -style height 20"
# ST_UTIL_CMD += "-ex=break src/stm32_node.cpp:45"
# ST_UTIL_CMD += "-ex=run"


all: clean build erase flash

${BUILD_DIR}/Makefile:
	cmake \
		-B${BUILD_DIR} \
		-DCMAKE_BUILD_TYPE=Debug \
		-DCMAKE_BUILD_TYPE=${BUILD_TYPE} \
		-DCMAKE_TOOLCHAIN_FILE=cmake/gcc-arm-none-eabi.cmake \
		-DCMAKE_EXPORT_COMPILE_COMMANDS=ON \
		-DDUMP_ASM=OFF

cmake: ${BUILD_DIR}/Makefile

build: cmake
	$(MAKE) -C ${BUILD_DIR} --no-print-directory

SRCS := $(shell find . -name '*.[ch]' -or -name '*.[ch]pp')
%.format: %
	clang-format -i $<
format: $(addsuffix .format, ${SRCS})

clean:
	rm -rf $(BUILD_DIR)

flash:
	-st-flash write  ${BUILD_DIR}/${TARGET}.bin 0x8000000

erase:
	-st-flash erase

debug:
# First of all start st-util in another
# terminal
	@if nc -z localhost ${ST_UTIL_PORT}; then \
	  arm-none-eabi-gdb-py ${BUILD_DIR}/${TARGET}.elf ${ST_UTIL_CMD}; \
	else \
	  echo "Please run the `st-util -m -n` first!"; \
	fi
